package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.stereotype.Component;

/**
 * Created by emacurb
 */
@Component
public class SeleniumHelper {

    private WebDriver driver;

    SeleniumHelper(WebDriver driver) {
        this.driver = driver;
    }

    public void focusOnElement(WebElement element) {
        if ("input".equals(element.getTagName())) {
            element.sendKeys("");
        } else {
            new Actions(driver).moveToElement(element).perform();
        }
    }
}
