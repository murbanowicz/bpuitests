package configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;


/**
 * Instruments beans, which are annotated by {@link PageObject}.
 *
 * <p>It calls {@link PageFactory#initElements(WebDriver, Object)}, which revives selenium annotations like {@link FindBy}.</p>
 *
 * @see PageObject
 */
@Component
public class PageObjectBeanPostProcessor implements BeanPostProcessor {

    @Autowired
    private WebDriver driver;

    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(PageObject.class)) {
            PageFactory.initElements(driver, bean);
        }
        return bean;
    }
}
