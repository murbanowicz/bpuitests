package configuration;

import org.springframework.stereotype.Component;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation, which indicates classes
 * designed as <a href="https://code.google.com/p/selenium/wiki/PageObjects">page object pattern</a>.
 *
 * @see PageObjectBeanPostProcessor
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface PageObject {
}
