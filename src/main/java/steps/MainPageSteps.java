package steps;

import configuration.WebDriverBean;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import pages.MainPage;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@CucumberContextConfiguration
@ContextConfiguration(classes = WebDriverBean.class)
public class MainPageSteps {

    @Autowired
    private MainPage mainPage;

    @Given("User is on main page")
    public void userIsOnMainPage() {
        assertNotNull(mainPage);
        mainPage.goToPage();
        mainPage.validateThatPageIsLoaded();
    }

    @When("User searches for {string} on main page")
    public void userSearchesForOnMainPage(String prodctName) {
        this.userTypesInSearchInputOnTheMainPage(prodctName);
        this.userClicksSearch();
    }

    @Then("User sees {string} on a list")
    public void userSeeOnAList(String prodctName) {
        mainPage.validateThatProductIsDisplayed(prodctName);
    }

    @When("User types {string} in search input on the main page")
    public void userTypesInSearchInputOnTheMainPage(String prodctName) {
        mainPage.search(prodctName);
    }

    @Then("Tooltip {string} is presented")
    public void tooltipIsPresented(String arg0) {
        assertTrue(mainPage.getSearchTip().isDisplayed());
    }

    @And("User clicks search")
    public void userClicksSearch() {
        mainPage.submitSearch();
    }

    @Then("Following message is displayed {string}")
    public void followingMessageIsDisplayedNonExistingProudct(String msg) {
        mainPage.validateThatProductIsNotDisplayed(msg);
    }

}
