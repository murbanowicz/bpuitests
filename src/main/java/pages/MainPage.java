package pages;

import configuration.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by emacurb
 */
@PageObject
public class MainPage {

    private WebDriver driver;

    @Autowired
    private WebDriverWait webDriverWait;

    @FindBy(id = "search_query_top")
    private WebElement searchIput;

    @FindBy(name = "submit_search")
    private WebElement searchButton;

    @FindBy(css = ".ac_results ul li")
    private WebElement searchTip;

    @FindBy(css = "a .logo[src='http://automationpractice.com/img/logo.jpg']")//TODO move url to properties
    private WebElement logo;

    @FindBy(css = "a.login")//TODO move url to properties
    private WebElement login;

    @FindBy(css = ".alert-warning")
    private WebElement alert;

    MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void goToPage() {
        driver.get("http://automationpractice.com");//TODO move url to properties
    }

    public MainPage search(String textToSearch) {
        searchIput.clear();
        searchIput.sendKeys(textToSearch);

        return this;
    }

    public MainPage submitSearch() {
        searchButton.click();

        return this;
    }

    public WebElement getSearchTip() {
        return searchTip;
    }

    public void validateThatPageIsLoaded() {
        assertTrue(searchIput.isDisplayed());
        assertTrue(searchButton.isDisplayed());
        assertTrue(logo.isDisplayed());
        assertTrue(login.isDisplayed());
        //TODO add more validations here
    }

    public void validateThatProductIsDisplayed(String productName) {
        assertTrue(driver.findElement(By.cssSelector("a.product_img_link[title=\"" + productName + "\"]")).isDisplayed());
    }

    public void validateThatProductIsNotDisplayed(String alertMsg) {
        //TODO validate that list is empty
        assertTrue(alert.isDisplayed());
        assertThat(alert.getText(), equalTo(alertMsg));
    }
}
