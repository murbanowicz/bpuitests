import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Created by emacurb
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources")
public class CucumberCukesTest {
}
