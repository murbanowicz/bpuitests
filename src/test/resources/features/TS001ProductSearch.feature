Feature: TS001 Product search

  Scenario Outline: TC001 Search for an existing proudct
    Given User is on main page
    When User types "<product>" in search input on the main page
    And User clicks search
    Then User sees "<product>" on a list
    Examples:
      | product   |
      |    Blouse |
      |    Printed Chiffon Dress |
      |    Faded Short Sleeve T-shirts |

  Scenario: TC002 Tooltip is displayed while searching
    Given User is on main page
    When User types "Blouse" in search input on the main page
    Then Tooltip "Blouse" is presented

  Scenario: TC003 Search for non-existing proudct
    Given User is on main page
    When User searches for "non-existing proudct" on main page
    Then Following message is displayed "No results were found for your search \"non-existing proudct\""

